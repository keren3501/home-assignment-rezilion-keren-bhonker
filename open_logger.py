#!/usr/bin/python

from __future__ import print_function
from bcc import ArgString, BPF
from bcc.utils import printb
import argparse
import ctypes as ct
from datetime import datetime, timedelta
import os
import sys

# define BPF program
bpf_text = """
#include <uapi/linux/ptrace.h>
#include <uapi/linux/limits.h>
#include <linux/sched.h>

struct val_t {
    u64 id;
    char comm[TASK_COMM_LEN];
    const char *fname;
};

struct data_t {
    u64 id;
    u64 ts;
    char fname[NAME_MAX];
};

BPF_HASH(infotmp, u64, struct val_t);
BPF_PERF_OUTPUT(events);

int trace_entry(struct pt_regs *ctx, int dfd, const char __user *filename, int flags)
{
    struct val_t val = {};
    u64 id = bpf_get_current_pid_tgid();
    
    if (bpf_get_current_comm(&val.comm, sizeof(val.comm)) == 0) {
        val.id = id;
        val.fname = filename;
       	infotmp.update(&id, &val);
    }

    return 0;
};

int trace_return(struct pt_regs *ctx)
{
    u64 id = bpf_get_current_pid_tgid();
    struct val_t *valp;
    struct data_t data = {};

    u64 tsp = bpf_ktime_get_ns();	//get the time stamp

    valp = infotmp.lookup(&id);
    if (valp == 0) {
        // missed entry
        return 0;
    }

    bpf_probe_read(&data.fname, sizeof(data.fname), (void *)valp->fname);
    data.id = valp->id;
    data.ts = tsp / 1000;

    events.perf_submit(ctx, &data, sizeof(data));
    infotmp.delete(&id);

    return 0;
}
"""

arguments = sys.argv
log_path = ""
#the correct way to run the program: ./open_logger --log_path <log_full_path>
#len(arguments) should be 3 and arguments[1] should be --log_path
if len(arguments) == 3:
	if arguments[1] == "--log_path":
		try:	
			log_path = open(arguments[2], 'w')
		except IOError:	#if we couldn't open the file
			print("Couldn't open the file...")
			exit()	#stop the program

		# initialize BPF
		b = BPF(text=bpf_text)
		b.attach_kprobe(event="do_sys_open", fn_name="trace_entry")
		b.attach_kretprobe(event="do_sys_open", fn_name="trace_return")

		TASK_COMM_LEN = 16
		NAME_MAX = 255

		class Data(ct.Structure):
			_fields_ = [
				("id", ct.c_ulonglong),
				("ts", ct.c_ulonglong),
				("fname", ct.c_char * NAME_MAX),
			]

		def print_event(cpu, data, size):
			event = ct.cast(data, ct.POINTER(Data)).contents

			#The trace functions give us the file's path, for example: /Downloads/rezilion
			#I want the file's name only (rezilion) so I need to find last slash in the string and cut it
			index_of_slash = (event.fname).rfind("/") + 1	#find the last slash in the path of the file
			if index_of_slash != -1:	#if there is a slash in the path
				event.fname = event.fname[index_of_slash: ]	#cut the string
			
			if (event.fname).find("rezilion") != -1:	#if the word "rezilion" is in the file's name
				log_path.write(str(float(event.ts)/ 1000000) + "\t" + event.fname + "\n")	#write to the log file


		# loop with callback to print_event
		b["events"].open_perf_buffer(print_event, page_cnt=64)
		while True:
			try:
				b.perf_buffer_poll()
			except KeyboardInterrupt:
				exit()
else:	#if the arguments' len wasn't 3
	print("INVALID INPUT")
