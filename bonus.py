#!/usr/bin/python

from __future__ import print_function
from bcc import ArgString, BPF
from bcc.utils import printb
import argparse
import ctypes as ct
from datetime import datetime, timedelta
import os
import sys

# define BPF program
bpf_text = """
#include <uapi/linux/ptrace.h>
#include <uapi/linux/limits.h>
#include <linux/sched.h>

struct val_t {
    u64 id;
    char comm[TASK_COMM_LEN];
};

BPF_PERF_OUTPUT(events);

int trace(struct pt_regs *ctx)
{
    struct val_t val = {};
    u64 id = bpf_get_current_pid_tgid();
    bpf_get_current_comm(&val.comm, sizeof(val.comm));
    val.id = id;
	events.perf_submit(ctx, &val, sizeof(val));
    return 0;
};
"""

arguments = sys.argv
#the correct way to run the program: ./bonus <elf_file_path>
#len(arguments) should be 2
file = "/tmp/awesome_file.txt"
if(len(arguments) == 2):
	track = arguments[1]
	#The elf_file_path is for example: /bin/ls, /bin/cat and etc.
	#In order to get the name of the command from the path, I need to find the last slash in the string.
	index_of_slash = track.rfind("/") + 1
	if index_of_slash != -1:	#if there is a slash in the path
		track = track[index_of_slash: ]	#cut the string
	
	# initialize BPF
	b = BPF(text=bpf_text)
	event_name = b.get_syscall_fnname("execve")
	b.attach_kretprobe(event=event_name, fn_name="trace")

	TASK_COMM_LEN = 16
	NAME_MAX = 255

	class Data(ct.Structure):
		_fields_ = [
			("id", ct.c_ulonglong),
			("comm", ct.c_char * TASK_COMM_LEN),
		]

	def print_event(cpu, data, size):
		event = ct.cast(data, ct.POINTER(Data)).contents

		if event.comm == track:	#if the command is the same command that I should track
			with open(file, 'w') as awesome_file:	#open a file names "awesome_file.txt"
				awesome_file.write("Learning never exhausts the mind")

	# loop with callback to print_event
	b["events"].open_perf_buffer(print_event, page_cnt=64)
	while True:
		try:
			b.perf_buffer_poll()
		except KeyboardInterrupt:
			exit()
else:
	print("INVALID INPUT")
